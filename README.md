# shine_mongol.com - Backend Django

This application uses python 3.5.

## Development

### Manual

Log in to postgresql cli as a superuser on database

```sh
sudo -u postgres psql
```

Create extensions so all new databases will have them (required for running tests)

```sh
# CREATE EXTENSION postgis;
# CREATE EXTENSION citext;
```

Create the database

```sh
# CREATE DATABASE <db_name>;
# CREATE USER <db_uname>;
# ALTER USER <db_uname> WITH superuser;
# ALTER USER <db_uname> WITH PASSWORD <db_password>;
```

Install python requirements

```sh
pip install -r requirements/local.txt
```

Install rabbitmq.
```sh
TODO you have to figure this out on your own.
```

```sh
virtualenv -p python3 cooliggy

gedit ~/.virtualenvs/shine_mongol/bin/activate
doorh hediig ene hayag deer bich.

export DJANGO_SETTINGS_MODULE=settings.local
export SECRET_KEY=notasecret
export DATABASE_NAME=<db_name>
export DATABASE_USER=<db_uname>
export DATABASE_PASSWORD=<db_password>
export DATABASE_HOST=127.0.0.1
export CELERY_BROKER_URL=<celery_broker_url> # odoohondoo yuu ch baij bolno
export DEBUG=true
export PYTHONPATH=~/<project_dir>/django_apps/
export FRONT_END_BASE_URL=http://localhost # odoohondoo yuu ch baij bolno
```
workon cooliggy
```sh

```
Migrate the database

```sh
./manage.py migrate
```

Run the server

```sh
./manage.py runserver
```

#### Running Tests

```sh
py.test
```

Upload image test

```sh
curl -X PATCH -H "Content-Type: multipart/form-data" -H "Authorization: Token 7nx4gUG1Vy1yoUxsqwnXlGxcWuc1TiMj1dAmukCd" -F "image=@/home/bx/q.jpg" http://127.0.0.1:8000/v1/user/me/ > out.html
```
