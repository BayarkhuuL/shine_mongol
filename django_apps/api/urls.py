from django.conf.urls import include, url

from dokie.views import schema_view


urlpatterns = [
    url(r'^v1/', include('api.v1.urls', namespace='v1')),
    url(r'^v1/docs/', schema_view),
]
