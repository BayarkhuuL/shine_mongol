from celery.decorators import task

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.core import signing
from django.template.loader import render_to_string



@task(name='email_reset_password')
def email_reset_password(user_id):
    User = get_user_model()
    user = User.objects.get(id=user_id)

    user_token = default_token_generator.make_token(user)
    token = signing.dumps([user.pk, user_token])

    subject = "Нууц үг шинэчлэх хүсэлт"
    body = render_to_string('emails/reset_password.txt', {
        'base_url': settings.FRONT_END_BASE_URL,
        'token': token,
    })
    user.email_user(subject, body)
