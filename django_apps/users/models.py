from django.contrib.auth.models import AbstractBaseUser
from django.core.mail import send_mail
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel
from model_utils import Choices

from .managers import UserManager
from utils.models import CaseInsensitiveEmailField, random_name_in


class PermissionsMixin(models.Model):
    """
    A mixin class that adds the fields and methods necessary to support
    Django's Permission model using the ModelBackend.
    """
    is_superuser = models.BooleanField(
        _('superuser status'), default=False,
        help_text=_("Designates that this user has all permissions without "
                    "explicitly assigning them.")
    )

    class Meta:
        abstract = True

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user is superadmin and is active
        """
        return self.is_active and self.is_superuser

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user is superadmin and is active
        """
        return self.is_active and self.is_superuser

    def has_module_perms(self, app_label):
        """
        Returns True if the user is superadmin and is active
        """
        return self.is_active and self.is_superuser

    @property
    def is_staff(self):
        return self.is_superuser


class User(TimeStampedModel, AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=200, blank=True)
    last_name = models.CharField(max_length=200, blank=True)
    image = models.ImageField(null=True, blank=True,
                              upload_to=random_name_in('user-images'))

    # Email, phone is unique only if not blank.
    # One of email or phone is required. See initial migration.
    email = CaseInsensitiveEmailField(unique=True)
    username = models.CharField(unique=True, max_length=50)

    is_active = models.BooleanField(
        _('active'), default=True, help_text=_(
            "Designates whether this user should be treated as "
            "active. Unselect this instead of deleting accounts."))

    objects = UserManager()

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return ''

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @cached_property
    def is_admin(self):
        return hasattr(self, 'admin_profile')

    @cached_property
    def is_moderator(self):
        return hasattr(self, 'moderator_profile')


class Moderator(User):
    user = models.OneToOneField(User, parent_link=True, related_name='moderator_profile')


class Admin(User):
    user = models.OneToOneField(User, parent_link=True,
                                related_name='admin_profile')


class RoleRequest(TimeStampedModel):
    user = models.ForeignKey(User,
                             related_name='role_requests',
                             related_query_name='role_request',
                             on_delete=models.CASCADE)
    body = models.TextField(blank=True)
    Roles = Choices(
        ('moderator', 'Moderator'),
        ('admin', 'Admin'),
    )
    role = models.CharField(max_length=10, choices=Roles)
