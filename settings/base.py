import os

from pathlib import Path
from datetime import timedelta

from django.core.exceptions import ImproperlyConfigured


def env(var_name):
    '''Get the environment variable or raise exception.'''
    try:
        return os.environ[var_name].encode('latin1').decode('unicode_escape')
    except KeyError:
        error_msg = "Set the {} environment variable".format(var_name)
        raise ImproperlyConfigured(error_msg)


# Base settings
BASE_DIR = Path(__file__).parent.parent
APPS_DIR = BASE_DIR / 'django_apps'

ALLOWED_HOSTS = ['*']
ROOT_URLCONF = 'urls'
WSGI_APPLICATION = 'wsgi.application'
SECRET_KEY = env('SECRET_KEY')
AUTH_USER_MODEL = 'users.User'
DEFAULT_FROM_EMAIL = 'shine_mongol.com'
SITE_NAME = 'shine_mongol'
ADMINS = (('Bayarkhuu', 'l.bayarkhuu@gmail.com'),)

REGISTER_VERIFY_MAX_AGE = timedelta(hours=24)
FILE_UPLOAD_HANDLERS = ['django.core.files.uploadhandler.TemporaryFileUploadHandler']
FRONT_END_BASE_URL = env('FRONT_END_BASE_URL')

# Email
EMAIL_HOST = 'lbayarkhuu.gmail.com'
EMAIL_HOST_USER = 'lbayarkhuu@gmail.com'
EMAIL_HOST_PASSWORD = 'password'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': env('DATABASE_NAME'),
        'USER': env('DATABASE_USER'),
        'PASSWORD': env('DATABASE_PASSWORD'),
        'HOST': env('DATABASE_HOST')
    }
}


# Applications
DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.postgres',
]
THIRD_PARTY_APPS = [
    'corsheaders',
    'rest_framework',
    'django_extensions',
    'timed_auth_token',
]
PROJECT_APPS = [
    'dokie',
    'api',
    'users',
]
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS


# Middleware
MIDDLEWARE_CLASSES = [
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]


# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [str(APPS_DIR / 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images) and media files (user uploads)
STATIC_URL = '/static/'
STATIC_ROOT = str(BASE_DIR / 'static')
STATICFILES_DIRS = [
    str(APPS_DIR / 'static')
]
MEDIA_URL = '/media/'
MEDIA_ROOT = str(BASE_DIR / 'media')


# Rest framework
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # Used for API clients.
        'timed_auth_token.authentication.TimedAuthTokenAuthentication',

        # Used for browsable API.
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
        'rest_framework.parsers.JSONParser',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'PAGE_SIZE': 25,
    'SEARCH_PARAM': 'q',
    'TEST_REQUEST_DEFAULT_FORMAT': 'json'
}

# Swagger settings
SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'basic': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    },
    'USE_SESSION_AUTH': False,
}

# Celery
CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_SEND_TASK_ERROR_EMAILS = True
CELERY_TASK_TIME_LIMIT = 30
CELERY_TASK_ROUTES = {
    'users.tasks.invite_company_admin': 'high-priority',
}


# CORS
CORS_ORIGIN_ALLOW_ALL = True
CORS_EXPOSE_HEADERS = (
    'Content-Length',
    'Content-Range',
    'Content-Encoding',
)
