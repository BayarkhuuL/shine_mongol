from .base import *


DEBUG = env('DEBUG').lower() == 'true'
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
