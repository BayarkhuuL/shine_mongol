import urllib.parse

from django.core.urlresolvers import reverse

import pytest


class ApiMixin:
    view_name = ''
    url_kwargs = None

    def reverse(self, view_name=None, params=None, **kwargs):
        # Specified in integration/vX/conftest.py
        version = pytest.api_version

        if self.url_kwargs is not None:
            kwargs.setdefault('kwargs', self.url_kwargs)

        if not view_name:
            view_name = self.view_name

        url = reverse('{}:{}'.format(version, view_name), **kwargs)

        if params:
            for param, value in params.items():
                if isinstance(value, list):
                    params[param] = ','.join(map(str, value))
            url += '?' + urllib.parse.urlencode(params)

        return url

    def admin_reverse(self, view_name=None, params=None, **kwargs):
        if self.url_kwargs is not None:
            kwargs.setdefault('kwargs', self.url_kwargs)

        if not view_name:
            view_name = self.view_name

        url = reverse(view_name, **kwargs)

        if params:
            for param, value in params.items():
                if isinstance(value, list):
                    params[param] = ','.join(map(str, value))
            url += '?' + urllib.parse.urlencode(params)

        return url
