from django.core.cache import cache

from .fixtures import *


def pytest_runtest_setup(item):
    cache.clear()
